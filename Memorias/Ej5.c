#include <stdio.h>
#include <stdint.h>

int32_t suma(int32_t a, int32_t b);

int main()
{
    int32_t v1=1;
    int32_t v2=2;
    printf("Dirección de v1: %p\n",&v1); 
    printf("Dirección de v2: %p\n",&v2);
    printf("%d",suma(v1,v2));
    return 0;
}

int32_t suma(int32_t a, int32_t b){
 
    printf("Dirección de a: %p\n",&a); 
    printf("Dirección de b: %p\n",&b);
    return a+b;
    
}
