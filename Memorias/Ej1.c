#include <stdio.h>
#include <stdint.h>

int main()
{
    
    printf("El tamaño del puntero int de 8 bits es de %d bytes\n", sizeof(int8_t *));
    printf("El tamaño del puntero int de 16 bits es de %d bytes\n", sizeof(int16_t *));
    printf("El tamaño del puntero int de 32 bits es de %d bytes\n", sizeof(int32_t *));
    printf("El tamaño del puntero float es de %d bytes\n", sizeof(float *));
    printf("El tamaño del puntero double es de %d bytes\n", sizeof(double *));
    return 0;
}

//Todos los punteros tienent tamaño 8bytes=64 bits, que es la arquitectura del procesador