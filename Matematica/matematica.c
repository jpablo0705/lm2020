#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include "matematica.h"

int32_t Sumar_Array(int16_t*x, int16_t xn){

int32_t resul = 0;
for (int i=0;i<xn;i++){
	resul+=*(x+i);
}

return resul;

}


int16_t multiplicar_sat(int16_t a, int16_t b){

int16_t Sat16_Max = pow (2, 15) - 1;
  int16_t Sat16_Min = -pow (2, 15);

  int16_t mult_sat;
  int32_t x = a * b;
  if (x > Sat16_Max)
    mult_sat = Sat16_Max;
  else if (x < Sat16_Min)
    mult_sat = Sat16_Min;
  else
    mult_sat = x;

  return mult_sat;

}




inline int16_t multiplicar_sat_inline(int16_t a, int16_t b){

int16_t Sat16_Max=pow(2,15)-1;
int16_t Sat16_Min=-pow(2,15);

int16_t mult_sat;
int32_t x=a*b;
if(x>Sat16_Max)
    mult_sat=Sat16_Max;
else if(x<Sat16_Min)
    mult_sat=Sat16_Min;
else
    mult_sat=x;
    
return mult_sat;

}