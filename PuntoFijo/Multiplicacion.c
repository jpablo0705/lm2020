#include <stdio.h>
#include <stdint.h>
void print_fixedpoint (int32_t x,int8_t frac);
int16_t multiplicar_q7 (int8_t q71,int8_t q72);
int32_t multiplicar_q15 (int16_t q151,int16_t q152);

int main()
{
    //int16_t x=0b10110101;
    //print_fixedpoint(x,5);
    //print_fixedpoint(x<<1,6); //imprime lo mismo
    
    int8_t q71=0b01000000;
    int8_t q72=q71;
    multiplicar_q7(q71,q72);
    
    int16_t q151=0b0001001100110011; //0.15
    int16_t q152=0b0110000000000000; //0.75
    //print_fixedpoint(q152,15);
    //multiplicar_q15(q151,q152);
    return 0;
}


int16_t multiplicar_q7 (int8_t q71,int8_t q72){
    int16_t q15=((q71*q72)<<1);
    print_fixedpoint(q15,15);
    return q15;
}


int32_t multiplicar_q15 (int16_t q151,int16_t q152){
    int32_t q31=((q151*q152)<<1);
    print_fixedpoint(q31,31);
    return q31;
}


void print_fixedpoint (int32_t x,int8_t frac){
    float f;
    f=(float)x/((float)(2<<(frac-1)));
    printf("%f",f);
}