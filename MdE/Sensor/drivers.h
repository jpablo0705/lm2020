extern int Estado_Sensor (void); // devuelve 0 si el sensor está inactivo y 1 si está activo
extern void Encender_Alarma (void); // Enciende la alarma
extern void Apagar_Alarma (void); // Apaga la alarma
extern int Medicion_Sensor (void); // entrega un número correspondiente a la medición del sensor.
extern int Encender_Luz (t_color luz); // Enciende la luz recibida como parámetro: ROJO o VERDE.
extern int Apagar_Luz (t_color luz); // Apaga la luz recibida como parámetro: ROJO o VERDE.