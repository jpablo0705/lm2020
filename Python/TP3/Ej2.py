import fpbinary as fp
import cuantizar as cuant
import mi_sen as ms
import mi_triang as mt
import matplotlib.pyplot as plt

#----------------------Señal senoidal s1--------------------------
Fs=1000
N=512
f0=115
A=1
ph0=0

#Señal original
s1,t=ms.mi_sen(A, f0, ph0, Fs, N)
plt.subplot(4,2,1)
plt.plot(t, s1, '.k', t, s1, '-b')
plt.title("Señal original")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])

#Cuantización Q7
plt.subplot(4,2,3)
s1q7=[cuant.fpgen(1,7,x) for x in s1]
plt.plot(t, s1q7, '.k', t, s1q7, '-b')
plt.title("Señal Q7")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])

plt.subplot(4,2,4)
errorq7=s1-s1q7
plt.plot(t, errorq7, '.k', t, errorq7, '-b')
plt.title("Error Q7")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])

#Cuantización Q15
s1q15=[cuant.fpgen(1,15,x) for x in s1]
plt.subplot(4,2,5)
plt.plot(t, s1q15, '.k', t, s1q15, '-b')
plt.title("Señal Q15")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])

plt.subplot(4,2,6)
errorq15=s1-s1q15
plt.plot(t, errorq15, '.k', t, errorq15, '-b')
plt.title("Error Q15")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])

#Cuantización Q31
s1q31=[cuant.fpgen(1,31,x) for x in s1]
plt.subplot(4,2,7)
plt.plot(t, s1q31, '.k', t, s1q31, '-b')
plt.title("Señal Q31")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])

plt.subplot(4,2,8)
errorq31=s1-s1q31
plt.plot(t, errorq31, '.k', t, errorq31, '-b')
plt.title("Error Q31")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])


#----------------------Señal triangular s2--------------------------
Fs=1000
N=512
f0=290
A=1
ph0=0
plt.figure()
#Señal original
s2,t=mt.mi_triang(A, f0, ph0, Fs, N)
plt.subplot(4,2,1)
plt.plot(t, s2, '.k', t, s2, '-b')
plt.title("Señal original")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])

#Cuantización Q7
plt.subplot(4,2,3)
s2q7=[cuant.fpgen(1,7,x) for x in s2]
plt.plot(t, s2q7, '.k', t, s2q7, '-b')
plt.title("Señal Q7")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])

plt.subplot(4,2,4)
errorq7s2=s2-s2q7
plt.plot(t, errorq7s2, '.k', t, errorq7s2, '-b')
plt.title("Error Q7")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])

#Cuantización Q15
s2q15=[cuant.fpgen(1,15,x) for x in s2]
plt.subplot(4,2,5)
plt.plot(t, s2q15, '.k', t, s2q15, '-b')
plt.title("Señal Q15")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])

plt.subplot(4,2,6)
errorq15s2=s2-s2q15
plt.plot(t, errorq15s2, '.k', t, errorq15s2, '-b')
plt.title("Error Q15")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])

#Cuantización Q31
s2q31=[cuant.fpgen(1,31,x) for x in s2]
plt.subplot(4,2,7)
plt.plot(t, s2q31, '.k', t, s2q31, '-b')
plt.title("Señal Q31")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])

plt.subplot(4,2,8)
errorq31s2=s2-s2q31
plt.plot(t, errorq31s2, '.k', t, errorq31s2, '-b')
plt.title("Error Q31")
plt.grid()
plt.xlabel("t[s]")
plt.xlim([0 ,0.25])


plt.show()