import math
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
def mi_triang(A, f0, ph0, Fs, N):
    k = np.arange(N)
    Ts = 1 / Fs
    t = k*Ts
    sen = A*signal.sawtooth(2*math.pi*f0*t,0.5)
    return sen, t


#triang,t=mi_triang(1,290,0,1000,512)
#plt.plot(t,triang)
#plt.xlim([0 ,0.05])
#plt.show()