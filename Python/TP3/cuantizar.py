import fpbinary as fp

def fpgen(entero,frac,val):
    val_fp = fp.FpBinary(int_bits=entero, frac_bits=frac, signed=True, value=val) #convierte val a su valor en punto fijo
    val_fpsw = fp.FpBinarySwitchable(fp_mode=True, fp_value=val_fp, float_value=val)
    return val_fpsw