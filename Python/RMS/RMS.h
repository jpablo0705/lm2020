#include <math.h>

int32_t multiplicar_q15(int16_t q151,int16_t q152);

int32_t RMS(int32_t*sgn,int16_t N){
    int32_t acum=0;
    for(int i=0;i<N;i++)
        acum=acum+multiplicar_q15((sgn[i]),(sgn[i]));
    return acum; //Está en Q15
}

int32_t multiplicar_q15 (int16_t q151,int16_t q152){
    int32_t q31=((q151*q152)<<1);
    int32_t aux=q31>>16; //truncar en Q15
    return aux;
}
