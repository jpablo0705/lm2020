#Recibe una señal fpSwitchable y devuelve su valor hexadecimal
import fpbinary as fp
import numpy as np

def fpsw2hex(fpsw):
    N=len(fpsw)
    FP=np.arange(N)
    FP=[fp.FpBinary(int_bits=1, frac_bits=15, signed=True, value=x) for x in fpsw]
    array = np.arange(0, N)
    FP_hex = [hex(FP[x].__index__()) for x in array]
    return FP_hex