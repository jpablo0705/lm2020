/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include "RMS.h"

int16_t s1[] = {
#include "s1.h"
};

int16_t s2[] = {
#include "s2.h"
};

int16_t s3[] = {
#include "s3.h"
};

int16_t s4[] = {
#include "s4.h"
};

int32_t RMS(int32_t*sgn,int16_t N);
float leer_fixedpoint (int32_t x,int8_t frac);

void main ()
{
  int16_t N1 = sizeof (s1) / 4;
  int16_t N2 = sizeof (s2) / 4;
  int16_t N3 = sizeof (s3) / 4;
  int16_t N4 = sizeof (s4) / 4;
  
  
 float rms[4]; 
 rms[0] = leer_fixedpoint(RMS(s1,N1),15)/N1; //Paso a float para la comparación con Python 
 rms[1] = leer_fixedpoint(RMS(s2,N2),15)/N2;
 rms[2] = leer_fixedpoint(RMS(s3,N3),15)/N3;
 rms[3] = leer_fixedpoint(RMS(s4,N4),15)/N4;


  FILE *rms_C;
  rms_C=fopen("rms_C.txt","wb");
  fprintf(rms_C,"%f\n%f\n%f\n%f",rms[0],rms[1],rms[2],rms[3]);
  fclose(rms_C);

  return;
}

float leer_fixedpoint (int32_t x,int8_t frac){
    float f;
    f=(float)x/((float)(2<<(frac-1)));
    return f;
}


