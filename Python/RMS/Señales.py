import cuantizar as cuant
import mi_sen as ms
import numpy as np
import RMS
import fpsw2hex
import math as m

#Generación de las 4 señales
Fs=1000
N=512
s1,t1=ms.mi_sen(1,50,0,Fs,N)
s2,t2=ms.mi_sen(0.75,100,0,Fs,N)
s3,t3=ms.mi_sen(0.5,200,0,Fs,N)
s4,t4=ms.mi_sen(0.15,400,0,Fs,N)

#Pasaje de las 4 señales de fp
s1q15=[cuant.fpgen(1,15,x) for x in s1]
s2q15=[cuant.fpgen(1,15,x) for x in s2]
s3q15=[cuant.fpgen(1,15,x) for x in s3]
s4q15=[cuant.fpgen(1,15,x) for x in s4]

#Pasaje de binario a hexa
s1q15_hex=fpsw2hex.fpsw2hex(s1q15)
s2q15_hex=fpsw2hex.fpsw2hex(s2q15)
s3q15_hex=fpsw2hex.fpsw2hex(s3q15)
s4q15_hex=fpsw2hex.fpsw2hex(s4q15)

#Guardado señales en un .h
np.savetxt("s1.h",s1q15_hex,fmt='%s',delimiter=',',newline=',') #El array está en formato %s
np.savetxt("s2.h",s2q15_hex,fmt='%s',delimiter=',',newline=',')
np.savetxt("s3.h",s3q15_hex,fmt='%s',delimiter=',',newline=',')
np.savetxt("s4.h",s4q15_hex,fmt='%s',delimiter=',',newline=',')

#Calculo RMS^2 en Python
RMS_Py=np.zeros(4)
RMS_Py[0]=RMS.RMS(s1)/N
RMS_Py[1]=RMS.RMS(s2q15)/N
RMS_Py[2]=RMS.RMS(s3q15)/N
RMS_Py[3]=RMS.RMS(s4q15)/N

#Importar RMS^2 calculado en C
RMS_C=np.zeros(4)
rms_C=open("rms_C.txt","r")
i=0
for x in rms_C.readlines():
    RMS_C[i]=x
    i=i+1
rms_C.close()

#Comparación RMS^2 Python vs C
print("--------S1--------\nReferencia:"+str(1/m.sqrt(2)**2)+"\nPython:"+str(RMS_Py[0])+"\nC:"+str(RMS_C[0])+"\nError Python-C:"+str(RMS_Py[0]-RMS_C[0]))
print("--------S2--------\nReferencia:"+str((0.75/m.sqrt(2))**2)+"\nPython:"+str(RMS_Py[1])+"\nC:"+str(RMS_C[1])+"\nError Python-C:"+str(RMS_Py[1]-RMS_C[1]))
print("--------S3--------\nReferencia:"+str((0.5/m.sqrt(2))**2)+"\nPython:"+str(RMS_Py[2])+"\nC:"+str(RMS_C[2])+"\nError Python-C:"+str(RMS_Py[2]-RMS_C[2]))
print("--------S4--------\nReferencia:"+str((0.15/m.sqrt(2))**2)+"\nPython:"+str(RMS_Py[3])+"\nC:"+str(RMS_C[3])+"\nError Python-C:"+str(RMS_Py[3]-RMS_C[3]))