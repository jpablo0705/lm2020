#Cálculo del valor N*RMS^2 de la señal sgn

def RMS(sgn):
    N = len(sgn)
    rms=0
    for x in range(N):
        rms=rms+(sgn[x]*sgn[x])

    return rms