from fpbinary import FpBinary, FpBinarySwitchable
import matplotlib.pyplot as plt
import numpy as np

#help(FpBinary)
#help(FpBinarySwitchable)
fp_num = FpBinary(int_bits=4, frac_bits=4, signed=True, value=2.5)
print(fp_num)
#plt.show();