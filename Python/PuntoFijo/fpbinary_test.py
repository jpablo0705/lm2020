#!/usr/bin/env python3

import fpbinary as fp
import mi_sen as ms
import matplotlib.pyplot as plt
from scipy.fft import fft
import numpy as np
import math as m

# -------------------- funcion para convertir a un solo valor a punto fijo  -----------------

def fpgen(val):
    val_fp = fp.FpBinary(int_bits=1, frac_bits=16, signed=True, value=val) #convierte val a su valor en punto fijo
    val_fpsw = fp.FpBinarySwitchable(fp_mode=True, fp_value=val_fp, float_value=val)
    return val_fpsw


# -------------------- test basico libreria -----------------

fp_num = fp.FpBinary(int_bits=4, frac_bits=4, signed=True, value=2.5)
print(fp_num)
#int (fp_num,2)

print("formato: ", fp_num.format)

fp_num.resize((1,4))
print("resize 1.4: ", fp_num)


prod = fp_num*fp_num
print("q4 x q4 = ", prod.format)

# -------------------- señales -----------------

A = 1
f0 = 120
ph0 = 0
Fs = 1000
N = 100


s1, t = ms.mi_sen(A, f0, ph0, Fs, N)

resFrec = Fs / (N - 1)
k = np.arange(N)
f = k * resFrec

nFils = 3
nCols = 2
# ============================

plt.subplot(nFils, nCols, 1)
plt.plot(t, s1, '.k', t, s1, '-b')
plt.title("Señal original")
plt.grid()
plt.xlabel("t[s]")

plt.subplot(nFils, nCols, 2)
S1 = np.log(abs(fft(s1)))
plt.plot(f, S1, '.k', f, S1, '-g')

# ============================

# convierto la señal a punto fijo
s1_fp = [fpgen(x) for x in s1]


plt.subplot(nFils, nCols, 3)
plt.plot(t, s1_fp, '.k', t, s1_fp, '-b')
plt.title("Señal cuantificada")
plt.grid()
plt.xlabel("t[s]")

plt.subplot(nFils, nCols, 4)
S1_fp = np.log(abs(fft(s1_fp)))
plt.plot(f, S1_fp, '.k', f, S1_fp, '-g')

# ============================
plt.subplot(nFils, nCols, 5)
error = s1 - s1_fp

plt.plot(t, error, '.k', t, error, '-b')
plt.title("Error de cuantificacion")
plt.grid()
plt.xlabel("t[s]")

plt.subplot(nFils, nCols, 6)
Error = abs(fft(error))
plt.plot(f, Error, '.k', f, Error, '-g')


plt.show()





