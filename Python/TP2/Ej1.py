import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sSenoidal1 as seno


[s1,t1]=seno.Sintetizar(1,100,0,100,1000)
[s2,t2]=seno.Sintetizar(1,100,0,128,1000)
[s3,t3]=seno.Sintetizar(1,145,0,128,1000)
[s4,t4]=seno.Sintetizar(1,450,0,128,1000)


plt.subplot(4,1,1)
plt.plot(t1,s1)
plt.grid()
plt.title('Grafico temporal original')

plt.subplot(4,1,2)
plt.plot(t2,s2)
plt.grid()
plt.title('Grafico temporal N=128')

plt.subplot(4,1,3)
plt.plot(t3,s3)
plt.grid()
plt.title('Grafico temporal f=145Hz')

plt.subplot(4,1,4)
plt.plot(t4,s4)
plt.grid()
plt.title('Grafico temporal f=450Hz')

plt.show()


#Gráfico frecuencial
plt.figure()

f1=np.fft.fftfreq(len(s1),t1[1]-t1[0])
S1=np.abs(np.fft.fft(s1))/len(s1)
plt.subplot(4,1,1)
plt.plot(f1,S1)
plt.grid()
plt.title('Grafico frecuencial original')

f2=np.fft.fftfreq(len(s2),t2[1]-t2[0])
S2=np.abs(np.fft.fft(s2))/len(s2)
plt.subplot(4,1,2)
plt.plot(f2,S2)
plt.grid()
plt.title('Grafico frecuencial N=128')

f3=np.fft.fftfreq(len(s3),t3[1]-t3[0])
S3=np.abs(np.fft.fft(s3))/len(s3)
plt.subplot(4,1,3)
plt.plot(f3,S3)
plt.grid()
plt.title('Grafico frecuencial f=145Hz')

f4=np.fft.fftfreq(len(s4),t4[1]-t4[0])
S4=np.abs(np.fft.fft(s4))/len(s4)
plt.subplot(4,1,4)
plt.plot(f4,S4)
plt.grid()
plt.title('Grafico frecuencial f=450Hz')

plt.show()


#s_frec=np.real(np.fft.fft(s))
#w=np.arange(0,2*np.pi,2*np.pi/N)
#f=w*fs/(2*np.pi)
#plt.subplot(2,1,2)
#plt.plot(f,s_frec)
#plt.grid()
#plt.title('Grafico frecuencial')


