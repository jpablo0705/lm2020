import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sSenoidal1 as seno

fs=500
N=1000
f0=1.67

A=[-0.670691,0.009505,0.00276,0.001028,0.000902,0.00002]


ph=[2.775687,-0.15793,-1.405200,-2.791570,2.51738]

#[s1,t]=seno.Sintetizar(A[1],f0,ph[0],N,fs)
#[s2,t]=seno.Sintetizar(A[2],2*f0,ph[1],N,fs)
#[s3,t]=seno.Sintetizar(A[3],3*f0,ph[2],N,fs)
#[s4,t]=seno.Sintetizar(A[4],4*f0,ph[3],N,fs)
#[s5,t]=seno.Sintetizar(A[5],5*f0,ph[4],N,fs)
#sgn=(A[0]/2)*np.ones(N)+s1+s2+s3+s4+s5

sgn=np.zeros(N)
for x in range(5):
    [s,t]=seno.Sintetizar(A[x+1],(x+1)*f0,ph[x],N,fs)
    sgn+=s

sgn=(A[0]/2)*np.ones_like(sgn)+sgn

plt.plot(t,sgn)
plt.title('Grafico temporal')
plt.grid()
plt.show()


plt.figure()
f=np.fft.fftfreq(len(sgn),t[1]-t[0])
S=np.abs(np.fft.fft(sgn))/len(sgn)
plt.plot(f,S,".")
plt.grid()
plt.title('Grafico frecuencial')
plt.show()