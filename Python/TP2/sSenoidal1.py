import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy import fftpack as f

def Sintetizar(A,f,phi,N,fs):
	Ts=1/fs
	t=np.arange(0,N)*Ts
	sgn=np.sin(2*np.pi*f*t+phi)*A
	return [sgn,t];

#Ejemplo de uso
#[s,t]=Sintetizar(1,50,5,400,600)

#Gráfico temporal
#plt.plot(t,s)

#Gráfico frecuencial
#f=np.fft.fftfreq(len(s),t[1]-t[0])
#S=np.abs(np.fft.fft(s))/len(s)
#plt.plot(f,S)

#plt.grid()
#plt.show()
