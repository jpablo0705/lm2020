import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy import signal

def Sierra(A,f,phi,N,fs):
	Ts=1/fs
	t=np.arange(0,N)*Ts
	sgn=signal.sawtooth(2*np.pi*f*t+phi)*A
	return [sgn,t];

[s1,t]=Sierra(1,50,2*np.pi,40,600)
[s2,t]=Sierra(1,50,0,40,600)
plt.plot(t,s1,t,s2)
plt.grid()
plt.show()
