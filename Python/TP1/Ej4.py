import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math as m



def potencia(buf):
	acum=0
	n=len(buf)
	for x in range(0,n):
		acum=acum+buf[x]**2
	acum=acum/n
	return acum	

def Ruido_Aditivo(buf,SNR):
	pot_sgnDb=20*m.log10(potencia(buf))
	pot_ruidoDb=pot_sgnDb-SNR
	pot_ruido_veces=10**(pot_ruidoDb/20)
	ruido_valormedio=0
	ruido_desvio=np.sqrt(pot_ruido_veces)
	ruido=np.random.normal(ruido_valormedio,ruido_desvio,len(buf))
	sgn_ruido=buf+ruido
	return sgn_ruido



#buf=[1,2,3,4]
#print(p.potencia(buf))
#plt.subplot(2,1,1)
#plt.plot(buf)
#plt.subplot(2,1,2)
#plt.plot(Ruido_Aditivo(buf,20))
#plt.show()
