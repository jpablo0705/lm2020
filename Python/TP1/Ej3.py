import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

vmin_sensor=-1
vmax_sensor=1
vmin_adc=0
vmax_adc=3.3

def Cuantificar(buf,N):
	#vmin_adc=0
	#vmax_adc=3.3
	resolucion=(vmax_adc-vmin_adc)/(2**N-1)
	s1=buf/resolucion
	s2=round(s1)*resolucion
	return s2


def EscalarSgn(buf):
	#calular ganancia G
	#calcular offset

	G=(vmax_adc-vmin_adc)/(vmax_sensor-vmin_sensor)
	offset=(vmax_adc-vmin_adc)/2
	s=G*presion+offset
	return s

presion=pd.read_csv("presion_tp1.csv")

Fs=500
Ts=1/Fs
n=len(presion)
k=np.arange(0,n)
t=k*Ts

s=EscalarSgn(presion)

d1=Cuantificar(s,8)
d2=Cuantificar(s,12)
d3=Cuantificar(s,24)

plt.subplot(3,1,1)
plt.plot(t,d1)
plt.grid()
plt.title("ADC 8 bits")

plt.subplot(3,1,2)
plt.plot(t,d2)
plt.grid()
plt.title("ADC 12 bits")

plt.subplot(3,1,3)
plt.plot(t,d3)
plt.grid()
plt.title("ADC 24 bits")


#grafico errores
plt.figure()

plt.subplot(3,1,1)
plt.plot(t,s-d1)
plt.grid()
plt.title("Error con ADC 8 bits")
plt.grid(2)

plt.subplot(3,1,2)
plt.plot(t,s-d2)
plt.grid()
plt.title("Error con ADC 12 bits")
plt.grid(2)

plt.subplot(3,1,3)
plt.plot(t,s-d3)
plt.grid()
plt.title("Error con ADC 24 bits")
plt.grid(2)

plt.show()

#conclusion: a mayor cantidad de bits, la señal de error tiene mayor cantidad de puntos (porque las dos señales tienen mayor cantidad de puntos),
#pero tiene menor rango de valores (error mas chico)
