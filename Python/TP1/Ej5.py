import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import csv
import S2
import Ej4

vmin_sensor=-1
vmax_sensor=1
vmin_adc=0
vmax_adc=3.3

def Cuantificar(buf,N):
	#vmin_adc=0
	#vmax_adc=3.3
	resolucion=(vmax_adc-vmin_adc)/(2**N-1)
	s1=buf/resolucion
	s2=round(s1)*resolucion
	return s2

from csv import reader
# read csv file as a list of lists
with open('presion_tp1.csv', 'r') as read_obj:
    # pass the file object to reader() to get the reader object
    csv_reader = reader(read_obj)
    # Pass reader object to list() to get a list of lists
    presion = list(csv_reader)
    
#Armo eje temporal
Fs=500
Ts=1/Fs
n=len(presion)
k=np.arange(0,n)
t=k*Ts

#Armo la señal s2
s2=S2.EscalarSgn(presion)

#Armo R1, R2 y R3
R1=Ej4.Ruido_Aditivo(s2,10)
R2=Ej4.Ruido_Aditivo(s2,3)
R3=Ej4.Ruido_Aditivo(s2,1)

#Simulo el ADC de 12 bits
N=12
R1_cuant=Cuantificar(R1,N)
R2_cuant=Cuantificar(R2,N)
R3_cuant=Cuantificar(R3,N)


#plt.show()