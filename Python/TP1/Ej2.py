import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


presion=pd.read_csv("presion_tp1.csv")

Fs=500
Ts=1/Fs
n=len(presion)
k=np.arange(0,n)
t=k*Ts

vmin_sensor=-1
vmax_sensor=1

vmin_adc=0
vmax_adc=3.3

#calular ganancia G
#calcular offset

G=(vmax_adc-vmin_adc)/(vmax_sensor-vmin_sensor)
offset=(vmax_adc-vmin_adc)/2

s2=G*presion+offset
plt.plot(t,s2)
plt.xlabel("t[s]")
plt.ylabel("p[mmHg]")
plt.title("grafico temporal")
plt.grid()
plt.show()

bits=12
resolADC=(vmax_adc-vmin_adc)/(2**bits-1)
k=5*(10**-6) #un dato del fabricante es 5uv/v/mmHg
vcc=3.3 #tensión de alimentación del sensor
resolSensor=resolADC/(k*vcc)

print("La resolucion es de ")
print(resolSensor)
print("mmHg") 
