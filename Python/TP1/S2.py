import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

vmin_sensor=-1
vmax_sensor=1
vmin_adc=0
vmax_adc=3.3

def EscalarSgn(buf):
	#calular ganancia G
	#calcular offset

	G=(vmax_adc-vmin_adc)/(vmax_sensor-vmin_sensor)
	offset=(vmax_adc-vmin_adc)/2
	s=G*buf+offset
	return s