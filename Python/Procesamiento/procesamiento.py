import fpbinary as fp
import cuantizar as cuant
import mi_sen as ms
import numpy as np
import fpsw2hex
import matplotlib.pyplot as plt

Fs=1000
N=512
f1=115
A1=0.8
f2=290
A2=0.3

s1,t1=ms.mi_sen(A1,f1,0,Fs,N)
s2,t2=ms.mi_sen(A2,f2,0,Fs,N)

#------------Punto flotante--------------
s3=np.zeros(N)
for x in range(N):
    s3[x]=s1[x]*s2[x]


plt.subplot(3,1,1)
plt.plot(t1,s3, '.k', t1, s3, '-b')
plt.title("Señal punto flotante")
plt.grid()
plt.xlabel("t[s]")

#------------Punto fijo--------------
s1q7=[cuant.fpgen(1,7,x) for x in s1]
s2q7=[cuant.fpgen(1,7,x) for x in s2]

s3q15=np.zeros(N)
s3q15_h=np.zeros(N)
for x in range(N):
    s3q15[x]=(s1q7[x]*s2q7[x]) #No usar <<1 , ya lo hace la librería automáticamente
    #s3q15_h[x]=(hex((s1q7[x]*s2q7[x]).__index__()))

plt.subplot(3,1,2)
plt.plot(t1,s3q15, '.k', t1, s3q15, '-b')
plt.title("Señal punto fijo Q15")
plt.grid()
plt.xlabel("t[s]")

#------------Error--------------
error=s3-s3q15
plt.subplot(3,1,3)
plt.plot(t1,error, '.k', t1,error, '-b')
plt.title("Error")
plt.grid()
plt.xlabel("t[s]")

#s3q15S=np.zeros(N)
#s3q15S=[fp.FpBinary(int_bits=1, frac_bits=15, signed=True, value=x) for x in s3q15]

#array=np.arange(0,N)
#s3q15_h=[hex(s3q15S[x].__index__()) for x in array]
s3q15_h=fpsw2hex.fpsw2hex(s3q15)
print(s3q15)
print(s3q15_h)

#Esto para guardar anda bien, si no se usa FpSwitchable
#array=np.arange(0,N)
#s3q15_h=[hex(s1q7[x]*s2q7[x].__index__()) for x in array] #Paso array en punto fijo a hexa
#np.savetxt("sgn.h",s3q15_h,fmt='%s') #El array está en formato %s
#print(s3q15_h)


plt.show()